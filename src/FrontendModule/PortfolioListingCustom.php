<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\FrontendModule;

use Memo\FoundationBundle\Module\FoundationModule;
use Memo\PortfolioBundle\Model\PortfolioModel;

class PortfolioListingCustom extends FoundationModule
{
    /**
     * Template
     * @var string
     */

    protected $strTemplate = 'ce_portfolio_listing';

    protected function compile()
    {
        $arrItems = unserialize($this->foundation_item_selection);

        // Retrieve Items
        $colItems = PortfolioModel::findMultipleByIds($arrItems);

        if ($this->size) {
            $this->imgSize = $this->size;
        }

        if (is_object($colItems)) {

            $arrItems = $this->parseItems($colItems);

            $this->Template->items = $arrItems;
        }

        if (gettype($this->Template->cssID) === 'array') {

            if (array_key_exists(0, $this->Template->cssID)) {

                $this->Template->cssID = $this->Template->cssID[0];

            } else {

                $this->Template->cssID = '';

            }

        }

        if ($this->customTpl) {
            $this->Template->strTemplate = $this->customTpl;
        }

    }
}
