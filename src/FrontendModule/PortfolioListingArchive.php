<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\FrontendModule;

use Memo\PortfolioBundle\Module\ModulePortfolioListing;

class PortfolioListingArchive extends ModulePortfolioListing
{
    /**
     * Template
     * @var string
     */

    protected $strTemplate = 'ce_portfolio_listing';

}
