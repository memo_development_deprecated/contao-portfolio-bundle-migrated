<?php
/**
 * @package   PortfolioBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\Command;

use Contao\CoreBundle\Framework\ContaoFramework;
use Memo\PortfolioBundle\Classes\Importer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportCommand extends Command
{
    protected static $defaultName = 'portfolio:import';
    protected $framework;
    private $io;
    private $rows = [];
    private $statusCode = 0;

    public function __construct(ContaoFramework $contaoFramework)
    {
        $this->framework = $contaoFramework;
        $this->framework->initialize();
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Portfolio-Einrträge aus einer Datei importieren');
        $this->addArgument('file', InputArgument::REQUIRED, 'File to import?');
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->statusCode = 0;
        $objImporter = new Importer($this->io);

        if ($objImporter->import($input->getArgument('file'))) {

            $this->statusCode = 1;
            $this->io->text('Error while importing the portfolio-entries');

        }

        return $this->statusCode;
    }
}
