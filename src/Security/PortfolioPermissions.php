<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\Security;

final class PortfolioPermissions
{
    public const USER_CAN_EDIT_ARCHIVE = 'contao_user.portfolios';
    public const USER_CAN_CREATE_ARCHIVES = 'contao_user.portfoliop.create';
    public const USER_CAN_DELETE_ARCHIVES = 'contao_user.portfoliop.delete';
}
