<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoPortfolioBundle extends Bundle
{
}

