<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\Model;

use Memo\FoundationBundle\Model\FoundationModel;

/**
 * Class PortfolioArchiveModel
 * @package Memo\PortfolioBundle\Model
 */
class PortfolioArchiveModel extends FoundationModel
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_memo_portfolio_archive';

}
