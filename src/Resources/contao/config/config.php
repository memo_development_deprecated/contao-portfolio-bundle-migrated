<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\System;
use Symfony\Component\HttpFoundation\Request;

$GLOBALS['BE_MOD']['content']['memo_portfolio'] = array
(
    'tables' => array(
        'tl_memo_portfolio_archive',
        'tl_memo_portfolio'
    ),
    'table' => array('Contao\TableWizard', 'importTable'),
);


/**
 * Content elements
 */
$GLOBALS['TL_CTE']['portfolio']['portfolio_listing_archive'] = 'Memo\PortfolioBundle\FrontendModule\PortfolioListingArchive';;
$GLOBALS['TL_CTE']['portfolio']['portfolio_listing_custom'] = 'Memo\PortfolioBundle\FrontendModule\PortfolioListingCustom';;

/**
 * Add front end modules
 */
$GLOBALS['FE_MOD']['memo_portfolio'] = array
(
    'portfolio_listing' => 'Memo\PortfolioBundle\Module\ModulePortfolioListing',
    'portfolio_reader' => 'Memo\PortfolioBundle\Module\ModulePortfolioReader',
);

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_memo_portfolio_archive'] = 'Memo\PortfolioBundle\Model\PortfolioArchiveModel';
$GLOBALS['TL_MODELS']['tl_memo_portfolio'] = 'Memo\PortfolioBundle\Model\PortfolioModel';

/**
 * Backend CSS
 */
if (System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest(System::getContainer()->get('request_stack')->getCurrentRequest() ?? Request::create(''))){
    $GLOBALS['TL_CSS'][] = 'bundles/memoportfolio/backend.css?v=2023';
}

/**
 * AutoItem for Translation of aliases
 */
$GLOBALS['TL_AUTO_ITEM'][] = 'alias';

/**
 * Add permissions
 */
$GLOBALS['TL_PERMISSIONS'][] = 'portfolios';
$GLOBALS['TL_PERMISSIONS'][] = 'portfoliop';
