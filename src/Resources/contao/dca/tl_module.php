<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Memo\PortfolioBundle\Model\PortfolioArchiveModel;
use Memo\PortfolioBundle\Model\PortfolioModel;
use Contao\Backend;

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['portfolio_listing'] = '{title_legend}, name, type;{source_legend}, foundation_archives, foundation_featured, categories_filter, categories_filter_type, sql_filter, foundation_order; {image_legend}, imgSize, imgSizeGallery;{filter_legend:hide}, categories;{detail_legend},jumpTo;{template_legend}, customTpl, foundation_item_template;{expert_legend:hide},cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['portfolio_reader'] = '{title_legend}, name, type;{config_legend}, imgSize, imgSizeGallery;{template_legend}, customTpl, foundation_item_template;{expert_legend:hide},cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['jumpTo']['eval']['tl_class'] = 'w50 clr';
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpTo']['label'] = &$GLOBALS['TL_LANG']['tl_module']['jumpToOverride'];

$GLOBALS['TL_DCA']['tl_module']['config']['onload_callback'][] = array('tl_module_portfolio', 'autoOverrideOptions');

/**
 * Class tl_module_portfolio
 */
class tl_module_portfolio extends Backend
{
    public function autoOverrideOptions($dc)
    {
        $objModuleItem = \Contao\ModuleModel::findByPk($dc->id);

        if (!is_null($objModuleItem) && !is_null($objModuleItem->type) && stristr($objModuleItem->type, 'portfolio')) {

            $GLOBALS['TL_DCA']['tl_module']['fields']['foundation_archives']['options_callback'] = array('tl_module_portfolio', 'getArchives');

            $GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_template']['options_callback'] = array('tl_module_portfolio', 'getTemplates');

            $GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_selection']['options_callback'] = array('tl_module_portfolio', 'getSelectionOptions');
        }

    }

    public function getArchives(Contao\DataContainer $dc)
    {
        $arrArchives = array();
        $colArchives = PortfolioArchiveModel::findAll();

        if(!$colArchives) {
            return $arrArchives;
        }

        while ($colArchives->next()) {
            $arrArchives[$colArchives->id] = $colArchives->title;
        }

        return $arrArchives;
    }

    public function getSelectionOptions(Contao\DataContainer $dc)
    {
        // If multiple, unserialize
        if (is_string($dc->activeRecord->foundation_archives)) {
            $arrArchives = unserialize($dc->activeRecord->foundation_archives);
        } else {
            $arrArchives = $dc->activeRecord->foundation_archives;
        }

        // Get all Items
        $arrItems = array();
        if (!empty($arrArchives)) {
            $colItems = PortfolioModel::findPublishedByPids($arrArchives);

            if (is_object($colItems)) {
                foreach ($colItems as $objItem) {
                    $arrItems[$objItem->id] = $objItem->title;
                }
            }
        }

        return $arrItems;
    }

    public function getTemplates(Contao\DataContainer $dc)
    {
        return self::getTemplateGroup('portfolio_');
    }
}
