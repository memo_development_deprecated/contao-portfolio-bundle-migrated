<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\DataContainer;
use Contao\System;
use Memo\MemoFoundationBundle\Classes\FoundationBackend;

/**
 * Table tl_memo_portfolio
 */
$GLOBALS['TL_DCA']['tl_memo_portfolio'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => Contao\DC_Table::class,
        'ptable' => 'tl_memo_portfolio_archive',
        'switchToEdit' => true,
        'enableVersioning' => true,
        'markAsCopy' => 'title',
        'onsubmit_callback' => array
        (
            array('memo.foundation.toolbox', 'setCoordinates'),
            array('memo.foundation.toolbox', 'generateMarker'),
        ),
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid,sorting' => 'index'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 4,
            'fields' => array('date'),
            'panelLayout' => 'filter;search;sort,limit',
            'headerFields' => array('title', 'alias'),
            'disableGrouping' => true,
            'child_record_callback' => array(
                'tl_memo_portfolio', 'listLayout'
            ),
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit',
            'copy',
            'cut',
            'delete',
            'feature' => array
            (
                'href' => 'act=toggle&amp;field=featured',
                'icon' => 'featured.svg',
            ),
            'toggle' => array
            (
                'href' => 'act=toggle&amp;field=published',
                'icon' => 'visible.svg',
                'showInHeader' => true
            ),
            'show',
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '
            {general_legend}, title, alias, categories, teaser;
            {date_legend},date;{meta_legend},robots,seo_title,seo_description,serpPreview;
            {detail_legend}, subtitle, description, url;
            {media_legend},singleSRC, multiSRC;
            {location_legend},address, lat, long;
            {publish_legend}, featured, published, start, stop;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'foreignKey' => 'tl_memo_portfolio_archive.id',
            'sql' => "int(10) unsigned NOT NULL",
            'relation' => array('type' => 'belongsTo', 'load' => 'lazy')
        ),
        'sorting' => array
        (
            'label' => &$GLOBALS['TL_LANG']['MSC']['sorting'],
            'sorting' => false,
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'date' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['date'],
            'default' => time(),
            'exclude' => true,
            'filter' => false,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'date', 'mandatory' => true, 'doNotCopy' => true, 'datepicker' => true, 'tl_class' => 'w50 wizard'),
            'load_callback' => array
            (
                array('memo.foundation.toolbox', 'loadDate')
            ),
            'sql' => "int(10) unsigned NOT NULL default 0"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['title'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'subtitle' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['subtitle'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => false,
                'maxlength' => 255,
                'tl_class' => 'clr w50'
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'alias' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['alias'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'rgxp' => 'alias',
                'doNotCopy' => true,
                'maxlength' => 255,
                'tl_class' => 'w50',
                'unique' => true
            ),
            'save_callback' => array
            (
                array('tl_memo_portfolio', 'generateAlias')
            ),
            'sql' => "varchar(255) BINARY NOT NULL default ''"
        ),
        'teaser' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['teaser'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array(
                'mandatory' => false,
                'rte' => 'tinyMCE',
                'tl_class' => 'clr',
            ),
            'sql' => "mediumtext NULL"
        ),
        'description' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['description'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array(
                'mandatory' => false,
                'rte' => 'tinyMCE',
                'tl_class' => 'clr',
            ),
            'sql' => "mediumtext NULL"
        ),
        'url' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['url'],
            'translate' => true,
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'rgxp' => 'url', 'decodeEntities' => true, 'maxlength' => 255, 'dcaPicker' => true, 'addWizardClass' => false, 'tl_class' => 'w50 clr'),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'singleSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['singleSRC'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'filesOnly' => true,
                'extensions' => '%contao.image.valid_extensions%',
                'fieldType' => 'radio'),
            'sql' => "binary(16) NULL"
        ),
        'multiSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['multiSRC'],
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => array(
                'multiple' => true,
                'fieldType' => 'checkbox',
                'orderField' => 'orderSRC',
                'files' => true,
                'extensions' => '%contao.image.valid_extensions%',
                'isGallery' => true,
                'tl_class' => 'clr m12'
            ),
            'sql' => "blob NULL"
        ),
        'markerSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['markerSRC'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'sql' => "binary(16) NULL"
        ),
        'orderSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['orderSRC'],
            'sql' => "blob NULL"
        ),
        'categories' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['categories'],
            'exclude' => true,
            'inputType' => 'multiColumnWizard',
            'eval' => array(
                'tl_class' => 'clr',
                'style' => 'width:710px; max-width: 100%;',
                'dragAndDrop' => false,
                'columnFields' => array(
                    'category_id' => array(
                        'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['category_id'],
                        'exclude' => true,
                        'inputType' => 'select',
                        'options_callback' => array('memo.foundation.category', 'getCategoriesByDC'),
                        'eval' => array(
                            'style' => 'width:250px',
                            'includeBlankOption' => true,
                            'multiple' => false,
                        ),
                    ),
                    'category_fk_meta' => array(
                        'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['category_fk_meta'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'eval' => array(
                            'style' => 'width:400px'
                        ),
                    ),
                ),
            ),
            'sql' => "blob NULL"
        ),
        'robots' => array
        (
            'exclude' => true,
            'search' => true,
            'inputType' => 'select',
            'options' => array(
                'index,follow',
                'index,nofollow',
                'noindex,follow',
                'noindex,nofollow'
            ),
            'eval' => array(
                'tl_class' => 'w50',
                'includeBlankOption' => true,
            ),
            'sql' => "varchar(32) NOT NULL default ''"
        ),
        'seo_title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['seo_title'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 128, 'tl_class' => 'clr'),
            'sql' => "varchar(128) NOT NULL default ''",
        ),
        'address' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['address'],
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long clr'),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'lat' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['lat'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'w50 clr'),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'long' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['long'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'inputType' => 'text',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'seo_description' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['seo_description'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array('mandatory' => false, 'maxlength' => 255, 'tl_class' => 'clr'),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'serpPreview' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['serpPreview'],
            'exclude' => true,
            'inputType' => 'serpPreview',
            'eval' => array('url_callback' => array('tl_memo_portfolio', 'getSerpUrl'), 'titleFields' => array('seo_title', 'title'), 'descriptionFields' => array('seo_description', 'teaser', 'description')),
            'sql' => null
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['published'],
            'exclude' => true,
            'toggle' => true,
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50 clr'
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'featured' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['featured'],
            'toggle' => true,
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50'
            ),
            'sql' => array('type' => 'boolean', 'default' => false)
        ),
        'start' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['start'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard clr'),
            'sql' => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['stop'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'),
            'sql' => "varchar(10) NOT NULL default ''"
        )
    )
);

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generatePortfolioDCA']) && \is_array($GLOBALS['TL_HOOKS']['generatePortfolioDCA'])) {
    foreach ($GLOBALS['TL_HOOKS']['generatePortfolioDCA'] as $callback) {
        $this->import($callback[0]);
        $GLOBALS['TL_DCA']['tl_memo_portfolio'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_portfolio'], $this);
    }
}

// Add multi-lang fields automatically by checking for the translate field
$objLanguageService = System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_portfolio');

$objToolboxService = System::getContainer()->get('memo.foundation.toolbox');
$objToolboxService->toggleDetailFieldsFromPalette('tl_memo_portfolio');

// Add team-link (if the team-bundle is installed)
if (class_exists('Memo\TeamBundle\Model\TeamModel')) {

    PaletteManipulator::create()
        ->addLegend('team_legend', 'publish_legend', PaletteManipulator::POSITION_BEFORE)
        ->addField('team_members', 'team_legend', PaletteManipulator::POSITION_APPEND)
        ->applyToPalette('default', 'tl_memo_portfolio');

    $GLOBALS['TL_DCA']['tl_memo_portfolio']['fields']['team_members'] = array(
        'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['team_members'],
        'exclude' => true,
        'inputType' => 'multiColumnWizard',
        'eval' => array(
            'tl_class' => 'clr',
            'style' => 'width:710px; max-width: 100%;',
            'dragAndDrop' => false,
            'columnFields' => array(
                'category_id' => array(
                    'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['team_id'],
                    'exclude' => true,
                    'inputType' => 'select',
                    'foreignKey' => 'tl_memo_team.title',
                    'eval' => array(
                        'style' => 'width:250px',
                        'includeBlankOption' => true,
                        'multiple' => false,
                    ),
                ),
                'category_fk_meta' => array(
                    'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio']['team_fk_meta'],
                    'exclude' => true,
                    'inputType' => 'text',
                    'eval' => array(
                        'style' => 'width:400px'
                    ),
                ),
            ),
        ),
        'sql' => "blob NULL"
    );

}

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generatePortfolioDCAComplete']) && \is_array($GLOBALS['TL_HOOKS']['generatePortfolioDCAComplete'])) {
    foreach ($GLOBALS['TL_HOOKS']['generatePortfolioDCAComplete'] as $callback) {
        $this->import($callback[0]);
        $GLOBALS['TL_DCA']['tl_memo_portfolio'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_portfolio'], $this);
    }
}

/**
 * Class tl_memo_portfolio
 */
class tl_memo_portfolio extends FoundationBackend
{
    public function getSerpUrl($objItem)
    {
        return $this->generateSerpUrl($objItem);
    }

    public function listLayout($row)
    {
        $strTitle = '<div class="tl_content_left">' . $row['title'];

        if (isset($row['address']) && $row['address'] != '') {
            $strTitle .= ' <span style="color:#b3b3b3; padding-left:3px;">[' . $row['address'] . ']</span>';
        }

        if (isset($row['categories'])) {
            $strTitle .= self::getCategoryLabels($row['categories']);
        }

        $strTitle .= '</div>';

        return $strTitle;
    }

    public function generateAlias($varValue, DataContainer $dc)
    {
        return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_portfolio', array('title'));
    }

    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
    }

    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        $this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_portfolio', 'published');
    }
}
