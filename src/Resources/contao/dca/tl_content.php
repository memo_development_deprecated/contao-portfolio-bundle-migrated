<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\ContentModel;
use Contao\Controller;
use Contao\System;
use Contao\Backend;

Controller::loadDataContainer('tl_module');
System::loadLanguageFile('tl_module');

/**
 * Table tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['portfolio_listing_archive'] = '{type_legend},type,headline; {source_legend}, foundation_archives, foundation_featured, categories_filter, categories_filter_type, sql_filter, foundation_order; {image_legend}, size; {template_legend:hide}, customTpl, foundation_item_template; {invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['portfolio_listing_custom'] = '{type_legend},type,headline; {source_legend}, size, foundation_archives, foundation_item_selection; {template_legend:hide}, customTpl, foundation_item_template ; {invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = array('tl_content_portfolio', 'autoOverrideOptions');

/**
 * Class tl_content
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_content_portfolio extends Backend
{
    public function autoOverrideOptions($dc)
    {
        $objContentItem = ContentModel::findByPk($dc->id);

        if (!is_null($objContentItem) && !is_null($objContentItem->type) && stristr($objContentItem->type, 'portfolio')) {

            $GLOBALS['TL_DCA']['tl_content']['fields']['foundation_archives']['options_callback'] = array('tl_module_portfolio', 'getArchives');

            $GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_template']['options_callback'] = array('tl_module_portfolio', 'getTemplates');

            $GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_selection']['options_callback'] = array('tl_module_portfolio', 'getSelectionOptions');
        }

    }
}
