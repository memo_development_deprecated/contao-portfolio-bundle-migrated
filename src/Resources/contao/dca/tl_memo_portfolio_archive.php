<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\BackendUser;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Image;
use Contao\Config;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\UserGroupModel;
use Memo\MemoFoundationBundle\Classes\FoundationBackend;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Table tl_memo_portfolio_archive
 */
$GLOBALS['TL_DCA']['tl_memo_portfolio_archive'] = array
(

    // Config
    'config' => array
    (
        'dataContainer' => Contao\DC_Table::class,
        'enableVersioning' => true,
        'ctable' => array(
            'tl_memo_portfolio'
        ),
        'switchToEdit' => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
        'onload_callback' => array
        (
            array('tl_memo_portfolio_archive', 'checkPermission')
        ),
        'oncreate_callback' => array
        (
            array('tl_memo_portfolio_archive', 'adjustPermissions')
        ),
        'oncopy_callback' => array
        (
            array('tl_memo_portfolio_archive', 'adjustPermissions')
        ),
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => 1,
            'fields' => array('title'),
            'flag' => 1,
            'panelLayout' => 'filter; search; limit'
        ),
        'label' => array
        (
            'fields' => array('title', 'alias'),
            'format' => '%s <span class="backend-info">[%s]</span>'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'href' => 'act=edit',
                'icon' => 'edit.svg',
                'button_callback' => array('tl_memo_portfolio_archive', 'editHeader')
            ),
            'children',
            'copy' => array
            (
                'href' => 'act=copy',
                'icon' => 'copy.svg',
                'button_callback' => array('tl_memo_portfolio_archive', 'copyItem')
            ),
            'delete' => array
            (
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
                'button_callback' => array('tl_memo_portfolio_archive', 'deleteItem')
            ),
            'toggle' => array
            (
                'href' => 'act=toggle&amp;field=published',
                'icon' => 'visible.svg',
                'showInHeader' => true
            ),
            'show'
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__' => array('addMarker'),
        'default' => '{general_legend}, title, alias, category_groups, jumpTo, jumpToListing, robots, teaser; {media_legend}, singleSRC, generateCoordinates, addMarker; {publish_legend}, published, start, stop;',
    ),

    // Subpalettes

    'subpalettes' => array
    (
        'addMarker' => 'markerSRC, markerFolder, markerWidthTrim, markerHeightTrim, markerOffsetX, markerOffsetY',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['title'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'category_groups' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['category_groups'],
            'exclude' => true,
            'filter' => false,
            'inputType' => 'select',
            'foreignKey' => 'tl_memo_category.title',
            'options_callback' => ['memo.foundation.category', 'getCategoryGroups'],
            'eval' => array(
                'multiple' => true,
                'chosen' => true,
                'tl_class' => 'clr long'
            ),
            'sql' => "blob NULL"
        ),
        'alias' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['alias'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'rgxp' => 'alias',
                'doNotCopy' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'save_callback' => array
            (
                array('tl_memo_portfolio_archive', 'generateAlias')
            ),
            'sql' => "varchar(255) BINARY NOT NULL default ''"
        ),
        'jumpTo' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['jumpTo'],
            'translate' => true,
            'exclude' => true,
            'inputType' => 'pageTree',
            'foreignKey' => 'tl_page.title',
            'eval' => array(
                'fieldType' => 'radio',
                'tl_class' => 'clr m12'
            ),
            'sql' => "int(10) unsigned NOT NULL default 0",
            'relation' => array(
                'type' => 'hasOne',
                'load' => 'lazy'
            )
        ),
        'jumpToListing' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['jumpToListing'],
            'translate' => true,
            'exclude' => true,
            'inputType' => 'pageTree',
            'foreignKey' => 'tl_page.title',
            'eval' => array(
                'fieldType' => 'radio',
                'tl_class' => 'clr'
            ),
            'sql' => "int(10) unsigned NOT NULL default 0",
            'relation' => array(
                'type' => 'hasOne',
                'load' => 'lazy'
            )
        ),
        'robots' => array
        (
            'exclude' => true,
            'search' => true,
            'inputType' => 'select',
            'options' => array(
                'index,follow',
                'index,nofollow',
                'noindex,follow',
                'noindex,nofollow'
            ),
            'eval' => array('tl_class' => 'w50'),
            'sql' => "varchar(32) NOT NULL default ''"
        ),
        'teaser' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['description'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array(
                'mandatory' => false,
                'rte' => 'tinyMCE',
                'tl_class' => 'clr',
            ),
            'sql' => "mediumtext NULL"
        ),
        'singleSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['singleSRC'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'filesOnly' => true,
                'extensions' => Config::get('validImageTypes'),
                'fieldType' => 'radio'
            ),
            'sql' => "binary(16) NULL"
        ),
        'addMarker' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['addMarker'],
            'translate' => false,
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'submitOnChange' => true
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'generateCoordinates' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['generateCoordinates'],
            'translate' => false,
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'tl_class' => 'w50',
                'submitOnChange' => true
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'markerSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerSRC'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'mandatory' => true,
                'filesOnly' => true,
                'extensions' => Config::get('validImageTypes'),
                'fieldType' => 'radio'
            ),
            'sql' => "binary(16) NULL"
        ),
        'markerFolder' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerFolder'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'mandatory' => true,
                'filesOnly' => false,
                'fieldType' => 'radio',
                'folderPicker' => true
            ),
            'sql' => "binary(16) NULL"
        ),
        'markerWidthTrim' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerWidthTrim'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'default' => 8,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'rgxp' => 'digit',
                'tl_class' => 'w50 clr'
            ),
            'sql' => "int(2) NOT NULL default '8'"
        ),
        'markerHeightTrim' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerHeightTrim'],
            'translate' => false,
            'exclude' => true,
            'filter' => false,
            'default' => 28,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'rgxp' => 'digit',
                'tl_class' => 'w50'
            ),
            'sql' => "int(2) NOT NULL default '28'"
        ),
        'markerOffsetX' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerOffsetX'],
            'translate' => false,
            'exclude' => true,
            'default' => 4,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'rgxp' => 'digit',
                'tl_class' => 'w50'
            ),
            'sql' => "int(2) NOT NULL default '4'"
        ),
        'markerOffsetY' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerOffsetX'],
            'translate' => false,
            'exclude' => true,
            'default' => 4,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'rgxp' => 'digit',
                'tl_class' => 'w50'
            ),
            'sql' => "int(2) NOT NULL default '4'"
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['published'],
            'translate' => false,
            'exclude' => true,
            'toggle' => true,
            'flag' => DataContainer::SORT_INITIAL_LETTER_ASC,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50'
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'start' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['start'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard clr'),
            'sql' => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['stop'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'),
            'sql' => "varchar(10) NOT NULL default ''"
        )
    )
);

// Add the translated-fields for all 'translate' dca-fields
$objLanguageService = System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_portfolio_archive');

/**
 * Class tl_memo_portfolio_archive
 */
class tl_memo_portfolio_archive extends FoundationBackend
{
    public function __construct()
    {
        parent::__construct();
        $this->import(BackendUser::class, 'User');
    }

    /**
     * Check permissions to edit table tl_memo_portfolio_archive
     *
     * @throws AccessDeniedException
     */
    public function checkPermission()
    {

        // Allow Admins
        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->portfolios) || !is_array($this->User->portfolios)) {
            $root = array(0);
        } else {
            $root = $this->User->portfolios;
        }

        // Get Group Permissions manually
        if (is_array($this->User->groups) && is_array($this->User->groups) && count($this->User->groups) > 0) {

            foreach ($this->User->groups as $intGroupID) {

                $objGroup = UserGroupModel::findByPk($intGroupID);

                if ($objGroup && $objGroup->portfolios) {

                    $arrPortfolios = unserialize($objGroup->portfolios);

                    if (is_array($arrPortfolios) && count($arrPortfolios) > 0) {
                        foreach ($arrPortfolios as $intArchive) {
                            if (!in_array($intArchive, $root)) {
                                $root[] = $intArchive;
                            }
                        }
                    }
                }

            }

        }

        $GLOBALS['TL_DCA']['tl_memo_portfolio_archive']['list']['sorting']['root'] = $root;

        // Check permissions to add archives
        if (!$this->User->hasAccess('create', 'portfoliop')) {
            $GLOBALS['TL_DCA']['tl_memo_portfolio_archive']['config']['closed'] = true;
            $GLOBALS['TL_DCA']['tl_memo_portfolio_archive']['config']['notCreatable'] = true;
            $GLOBALS['TL_DCA']['tl_memo_portfolio_archive']['config']['notCopyable'] = true;
        }


        // Check permissions to delete calendars
        if (!$this->User->hasAccess('delete', 'portfoliop')) {
            $GLOBALS['TL_DCA']['tl_memo_portfolio_archive']['config']['notDeletable'] = true;
        }

        // Get request (contao 5+)
        $objRequest = System::getContainer()->get('request_stack')->getCurrentRequest();

        // Get session (contao 5+)
        $objSession = $objRequest->getSession();

        // Check current action
        switch (Input::get('act')) {
            case 'select':
                // Allow
                break;

            case 'create':
                if (!$this->User->hasAccess('create', 'portfoliop')) {
                    throw new AccessDeniedException('Not enough permissions to create portfolio archives.');
                }
                break;

            case 'edit':
            case 'copy':
            case 'delete':
            case 'show':
                if (!in_array(Input::get('id'), $root) || (Input::get('act') == 'delete' && !$this->User->hasAccess('delete', 'portfoliop'))) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' portfolio archive ID ' . Input::get('id') . '.');
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
            case 'copyAll':
                $session = $objSession->all();

                if (Input::get('act') === 'deleteAll' && !$this->User->hasAccess('delete', 'portfoliop')) {
                    $session['CURRENT']['IDS'] = array();
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array)$session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                if (Input::get('act')) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' portfolios archives.');
                }
                break;
        }
    }

    /**
     * Add the new archive to the permissions
     *
     * @param $insertId
     */
    public function adjustPermissions($insertId)
    {
        // The oncreate_callback passes $insertId as second argument
        if (func_num_args() == 4) {
            $insertId = func_get_arg(1);
        }

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->portfolios) || !is_array($this->User->portfolios)) {
            $root = array(0);
        } else {
            $root = $this->User->portfolios;
        }

        // The archive is enabled already
        if (in_array($insertId, $root)) {
            return;
        }

        /** @var AttributeBagInterface $objSessionBag */
        $objSessionBag = System::getContainer()->get('session')->getBag('contao_backend');

        $arrNew = $objSessionBag->get('new_records');

        if (is_array($arrNew['tl_memo_portfolio_archive']) && in_array($insertId, $arrNew['tl_memo_portfolio_archive'])) {
            // Add the permissions on group level
            if ($this->User->inherit != 'custom') {
                $objGroup = $this->Database->execute("SELECT id, portfolios, portfoliop FROM tl_user_group WHERE id IN(" . implode(',', array_map('\intval', $this->User->groups)) . ")");

                while ($objGroup->next()) {
                    $arrPortfoliop = StringUtil::deserialize($objGroup->portfoliop);

                    if (is_array($arrPortfoliop) && in_array('create', $arrPortfoliop)) {
                        $arrPortfolios = StringUtil::deserialize($objGroup->portfolios, true);
                        $arrPortfolios[] = $insertId;

                        $this->Database->prepare("UPDATE tl_user_group SET portfolios=? WHERE id=?")
                            ->execute(serialize($arrPortfolios), $objGroup->id);
                    }
                }
            }

            // Add the permissions on user level
            if ($this->User->inherit != 'group') {
                $objUser = $this->Database->prepare("SELECT portfolios, portfoliop FROM tl_user WHERE id=?")
                    ->limit(1)
                    ->execute($this->User->id);

                $arrPortfoliop = StringUtil::deserialize($objUser->portfoliop);

                if (is_array($arrPortfoliop) && in_array('create', $arrPortfoliop)) {
                    $arrPortfolios = StringUtil::deserialize($objUser->portfolios, true);
                    $arrPortfolios[] = $insertId;

                    $this->Database->prepare("UPDATE tl_user SET portfolios=? WHERE id=?")
                        ->execute(serialize($arrPortfolios), $this->User->id);
                }
            }

            // Add the new element to the user object
            $root[] = $insertId;
            $this->User->portfolios = $root;
        }
    }

    /**
     * Return the copy archive button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function copyItem($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->User->hasAccess('create', 'portfoliop') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
    }

    /**
     * Return the delete archive button
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function deleteItem($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->User->hasAccess('delete', 'portfoliop') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
    }

    public function editHeader($row, $href, $label, $title, $icon, $attributes)
    {
        return '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . Contao\StringUtil::specialchars($title) . '"' . $attributes . '>' . Contao\Image::getHtml($icon, $label) . '</a> ';
    }

    public function generateAlias($varValue, DataContainer $dc)
    {
        return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_portfolio_archive');
    }

    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
    }

    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        $this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_portfolio_archive', 'published');
    }

}
