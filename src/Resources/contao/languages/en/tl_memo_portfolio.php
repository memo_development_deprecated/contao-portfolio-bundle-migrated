<?php declare(strict_types=1);
/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_portfolio']['category_id'] = array('Category', 'Category that is assigned');
