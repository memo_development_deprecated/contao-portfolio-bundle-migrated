<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['memo_portfolio'] = array('Portfolios', 'Portfolio verwalten');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['portfolio_listing'] = array('Portfolio Listenansicht');
$GLOBALS['TL_LANG']['FMD']['portfolio_reader'] = array('Portfolio Detailansicht');
$GLOBALS['TL_LANG']['FMD']['memo_portfolio'] = array('Portfolio');
