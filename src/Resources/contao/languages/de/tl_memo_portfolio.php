<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */
/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_portfolio']['general_legend'] = 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_portfolio']['date_legend'] = 'Datum / Sortierung';
$GLOBALS['TL_LANG']['tl_memo_portfolio']['meta_legend'] = 'Metadaten';
$GLOBALS['TL_LANG']['tl_memo_portfolio']['detail_legend'] = 'Details';
$GLOBALS['TL_LANG']['tl_memo_portfolio']['media_legend'] = 'Medien';
$GLOBALS['TL_LANG']['tl_memo_portfolio']['location_legend'] = 'Standort';
$GLOBALS['TL_LANG']['tl_memo_portfolio']['team_legend'] = 'Personen';
$GLOBALS['TL_LANG']['tl_memo_portfolio']['publish_legend'] = 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_portfolio']['title'] = array('Titel', 'Titel des Eintrags');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['alias'] = array('Alias', 'Wie soll der Alias des Eintrags lauten? Der Alias muss eindeutig sein und wird für die URL verwendet.');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['categories'] = array('Kategorie(n)', 'Hinterlegen Sie die entsprechende/n Kategorie/n für diesen Eintrag');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['category_id'] = array('Kategorie', 'Kategorie die zugewiesen wird');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['category_fk_meta'] = array('Kategorie Zusatz', 'Falls Zusatzdaten pro Kategorie-Zuweisung benötigt werden, können diese hier hinterlegt werden.');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['teaser'] = array('Teaser', 'Eine kurze Beschreibung für die Listenansicht');

$GLOBALS['TL_LANG']['tl_memo_portfolio']['date'] = array('Datum', 'Datum des Eintrags (z.B. Projektabschluss) jenes für die Ausgabe und/oder Sortierung verwendet wird.');

$GLOBALS['TL_LANG']['tl_memo_portfolio']['subtitle'] = array('Untertitel', 'Untertitel des Eintrags');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['description'] = array('Beschreibung', 'Eine ausführliche Beschreibung für eine eventuelle Detailseite');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['url'] = array('Link', 'Intern oder externer Link für diesen Eintrag - z.B. für Referenzen');

$GLOBALS['TL_LANG']['tl_memo_portfolio']['singleSRC'] = array('Bild', 'Hauptbild für den Eintrag');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['multiSRC'] = array('Galeriebilder', 'Bilder für eine Galerie / Slider etc.');

$GLOBALS['TL_LANG']['tl_memo_portfolio']['robots'] = array('Robots-Tag', 'Hier legen Sie fest, wie Suchmaschinen und die Sitemap die Seite behandeln.');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['seo_title'] = array('SEO Titel', 'Suchmaschinen Titel des Eintrags - hiermit kann der Standard-Titel überschrieben werden. Optimaler Weise max. 60 Zeichen');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['seo_description'] = array('SEO Text', 'Suchmaschinen Text des Eintrags - hiermit kann der Standard-Text überschrieben werden. Optimaler Weise max. 160 Zeichen');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['serpPreview'] = array('Google Suchergebnis-Vorschau', 'Hier können Sie sehen wie Google die Metadaten in den Suchergebnissen anzeigt. Andere Suchmaschinen zeigen gegebenenfalls längere Texte an oder beschneiden diese an einer anderen Position.');

$GLOBALS['TL_LANG']['tl_memo_portfolio']['address'] = array('Adresse', 'Adresse des Standorts (in der Schweiz)');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['lat'] = array('Latitude', 'Latitude Koordinate (sofern ein Google Maps Key vorhanden ist, wird dieser Wert automatisch aus der Adresse generiert)');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['long'] = array('Longitude', 'Longitude Koordinate (sofern ein Google Maps Key vorhanden ist, wird dieser Wert automatisch aus der Adresse generiert)');

$GLOBALS['TL_LANG']['tl_memo_portfolio']['team_members'] = array('Beteiligte Personen', 'Welche Personen waren am Projekt beteiligt?');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['team_id'] = array('Person', 'Welche Person waren am Projekt beteiligt?');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['team_fk_meta'] = array('Person Zusatz', 'Wie war die Person beteiligt? (Möglichkeit, z.B. Position oder the Funktion im Projekt zu definieren)');

$GLOBALS['TL_LANG']['tl_memo_portfolio']['featured'] = array('Hervorgehoben', 'Soll dieser Eintrag auf der Webseite hervorgehoben werden?');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['published'] = array('Veröffentlicht', 'Soll dieser Eintrag auf der Webseite veröffentlicht werden?');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['start'] = array('Anzeigen ab', 'Wenn Sie den Eintrag erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['stop'] = array('Anzeigen bis', 'Wenn Sie den Eintrag nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_portfolio']['new'] = array('Neuer Eintrag', 'Neuer Eintrag anlegen');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['show'] = array('Details', 'Infos zum Eintrag mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['edit'] = array('Eintrag bearbeiten ', 'Eintrag bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['cut'] = array('Eintrag Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['copy'] = array('Eintrag Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_memo_portfolio']['delete'] = array('Eintrag Löschen ', 'ID %s löschen');
