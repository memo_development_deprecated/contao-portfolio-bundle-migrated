<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_user']['portfolio_legend'] = 'Portfolio-Rechte';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_user']['portfolios'] = array('Erlaubte Archive', 'Hier können Sie den Zugriff auf ein oder mehrere Portfolio-Archive erlauben.');
$GLOBALS['TL_LANG']['tl_user']['portfoliop'] = array('Archivrechte', 'Hier können Sie die Archivrechte festlegen.');
