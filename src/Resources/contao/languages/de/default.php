<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Miscellaneous
 */

$GLOBALS['TL_LANG']['MSC']['portfolioPicker'] = 'Portfolios';

/**
 * Content elements
 */
$GLOBALS['TL_LANG']['CTE']['portfolio'] = 'Portfolio';
$GLOBALS['TL_LANG']['CTE']['portfolio_listing_custom'] = array('Portfolio Liste (Manuell)', 'Manuell ausgewählte Portfolio-Einträge anzeigen.');
$GLOBALS['TL_LANG']['CTE']['portfolio_listing_archive'] = array('Portfolio Liste (Filter)', 'Ausgabe der Portfolio Einträge nach Archiv, Kategorie(n) oder SQL-Filter');
