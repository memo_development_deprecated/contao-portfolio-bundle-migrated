<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['general_legend'] = 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['media_legend'] = 'Medien';
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['publish_legend'] = 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['title'] = array('Titel', 'Name des Portfolios');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['alias'] = array('Alias', 'Wie soll der Alias des Eintrags lauten? Der Alias muss eindeutig sein und wird als URL Alias verwendet.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['jumpTo'] = array('Detailseite', 'Auf welcher Seite wurde das Reader-Modul eingebunden?');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['jumpToListing'] = array('Listenseiten', 'Auf welcher Seite wurde das Listen-Modul eingebunden?');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['robots'] = array('Robots-Tag', 'Hier legen Sie fest, wie Suchmaschinen und die Sitemap die Seite behandeln.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['category_groups'] = array('Kategorie Filter', 'Grenzen Sie die verfügbaren Kategorie-Gruppen für Kindelemente dieses Archives ein. Kind-Elemente können nur Kategorien von den hier definierten Gruppen erhalten. Leer = Alle Kategorien sind verfügbar.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['description'] = array('Beschreibung', 'Beschreibung des Eintrags. Wird auf der Detailseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['teaser'] = array('Teaser', 'Kurze Beschreibung des Eintrags. Wird auf der Übersichtsseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['singleSRC'] = array('Bild', 'Bild des Eintrags. Wird auf der Übersichtsseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['addMarker'] = array('Marker generieren?', 'Sollen für die Einträge automatisch Marker generiert werden?');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['generateCoordinates'] = array('Koordinaten generieren?', 'Sollen für die Einträge automatisch Koordinaten generiert werden? Voraussetzung ist die Angabe eines Google Maps API Key in den Einstellungen.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerSRC'] = array('Hintergrundbild', 'Bild jenes hinter das Element-Bild gelegt wird. Beispiel wäre bei Fortimo oder Wäspe & Partner');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerWidthTrim'] = array('Breiten Abzug', 'Um wie viel kleiner soll das Portfolio-Bild sein, als das Hintergrundbild? in Pixel.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerHeightTrim'] = array('Höhen Abzug', 'Um wie viel kleiner soll das Portfolio-Bild sein, als das Hintergrundbild? in Pixel.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerOffsetX'] = array('X-Offset', 'Um wie viel Pixel soll das Portfolio-Bild nach rechts verschoben werden?');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['markerOffsetY'] = array('Y-Offset', 'Um wie viel Pixel soll das Portfolio-Bild nach unten verschoben werden?');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['published'] = array('Veröffentlicht', 'Soll dieser Eintrag auf der Webseite veröffentlicht werden.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['start'] = array('Anzeigen ab', 'Wenn Sie das Portfolio erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['stop'] = array('Anzeigen bis', 'Wenn Sie das Portfolio nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['new'] = array('Neues Portfolio', 'Neue Portfolio anlegen');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['show'] = array('Details', 'Infos zum Portfolio mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['edit'] = array('Portfolio-Einträge bearbeiten ', 'Portfolio-Einträge bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['editheader'] = array('Portfolio bearbeiten ', 'Portfolio bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['cut'] = array('Portfolio Verschieben', 'Portfolio mit der ID %s verschieben');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['copy'] = array('Portfolio Duplizieren ', 'Portfolio mit der ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_memo_portfolio_archive']['delete'] = array('Portfolio Löschen ', 'Portfolio mit der ID %s löschen');
