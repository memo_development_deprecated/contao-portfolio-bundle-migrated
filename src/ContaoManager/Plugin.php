<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\ContaoManager;

use Contao\CalendarBundle\ContaoCalendarBundle;
use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\NewsBundle\ContaoNewsBundle;
use Memo\CategoryBundle\MemoCategoryBundle;
use Memo\FoundationBundle\MemoFoundationBundle;
use Memo\PortfolioBundle\MemoPortfolioBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(MemoPortfolioBundle::class)
                ->setLoadAfter([
                    ContaoCoreBundle::class,
                    ContaoCalendarBundle::class,
                    ContaoNewsBundle::class,
                    MemoCategoryBundle::class,
                    MemoFoundationBundle::class
                ])
                ->setReplace(['portfolio']),
        ];
    }
}
