<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\EventListener;

use Contao\Input;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Memo\FoundationBundle\EventListener\FoundationHookListener;
use Memo\PortfolioBundle\Model\PortfolioArchiveModel;
use Memo\PortfolioBundle\Model\PortfolioModel;
use Terminal42\ChangeLanguage\Event\ChangelanguageNavigationEvent;

class HookListener extends FoundationHookListener
{
    /**
     * @Hook("changelanguageNavigation")
     */
    public function onChangelanguageNavigation(ChangelanguageNavigationEvent $objEvent)
    {
        // Gather some information
        $objTargetRoot = $objEvent->getNavigationItem()->getRootPage();
        $objTarget = $objEvent->getNavigationItem()->getTargetPage();
        $strTargetLanguage = $objTargetRoot->rootLanguage;
        $strAlias = Input::get('auto_item');
        $colItem = null;

        if ($strAlias) {
            $colItem = PortfolioModel::getItemByTranslatedAlias($strAlias);
        }
        if ($colItem) {
            $arrDetailPageIds = PortfolioArchiveModel::getDetailPages(true, true);
        } else {
            return;
        }
        $objNavigationItem = $objEvent->getNavigationItem();

        // Detect item for the current page, e.g. to do nothing
        if ($objNavigationItem->isCurrentPage()) {
            return;
        }

        // Only act on auto_item and on jumpToPages
        if ($strAlias != '' && in_array($objTarget->id, $arrDetailPageIds)) {
            $colItem[0]->getTranslatedModel($strTargetLanguage);

            if ($colItem[0]->alias != '') {
                if ($objEvent->getUrlParameterBag()->hasUrlAttribute('items')) {
                    $objEvent->getUrlParameterBag()->setUrlAttribute('items', $colItem[0]->alias);
                } else {
                    $objEvent->getUrlParameterBag()->setUrlAttribute('alias', $colItem[0]->alias);
                }
            }

        }
    }
}
