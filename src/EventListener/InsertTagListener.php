<?php

declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\PageModel;
use Contao\System;
use Memo\PortfolioBundle\Model\PortfolioArchiveModel;
use Memo\PortfolioBundle\Model\PortfolioModel;

class InsertTagListener
{
    /**
     * @Hook("replaceInsertTags")
     */
    public function __invoke(
        string $insertTag,
        bool   $useCache,
        string $cachedValue,
        array  $flags,
        array  $tags,
        array  $cache,
        int    $_rit,
        int    $_cnt
    )
    {
        if (stristr($insertTag, 'portfolio_url')) {

            // Get ID from Inserttag
            $arrElements = explode('::', $insertTag);
            $intID = $arrElements[1];

            // ID defined
            if ($intID) {

                // Try and get Item
                if ($objPortfolioItem = PortfolioModel::findById($intID)) {

                    return $objPortfolioItem->getURL();
                }
            }
        }

        return false;
    }
}
