<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\EventListener;

use Contao\PageModel;
use Memo\FoundationBundle\EventListener\FoundationHookListener;
use Contao\CoreBundle\Event\ContaoCoreEvents;
use Contao\CoreBundle\Event\SitemapEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(ContaoCoreEvents::SITEMAP)]
class SitemapListener
{
    public function __invoke(SitemapEvent $objEvent): void
    {
        $objSitemap = $objEvent->getDocument();
        $objUrlSet = $objSitemap->childNodes[0];
        $intRootPageIds = $objEvent->getRootPageIds();

        $arrAdditionalPages = array();
        if($intRootPageIds){
            foreach($intRootPageIds as $intRootID){

                $objRootPage = PageModel::findByPk($intRootID);

                if($objRootPage){
                    $strLanguage = $objRootPage->rootLanguage;

                    $arrPages = FoundationHookListener::generateSitemapOrSearchIndex($arrAdditionalPages, 'tl_memo_portfolio', true , $strLanguage, $intRootID, true);
                    $arrAdditionalPages = array_merge($arrAdditionalPages, $arrPages);
                }

            }
        }

        if(is_array($arrAdditionalPages) && count($arrAdditionalPages) > 0){

            foreach($arrAdditionalPages as $strURL){
                $objLoc = $objSitemap->createElement('loc');
                $objLoc->appendChild($objSitemap->createTextNode($strURL));
                $objURLElement = $objSitemap->createElement('url');
                $objURLElement->appendChild($objLoc);
                $objUrlSet->appendChild($objURLElement);
            }

        }
    }
}
