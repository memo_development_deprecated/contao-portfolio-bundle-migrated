<?php
/**
 * @package   PortfolioBundle
 * @author      Rory Zuend, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\Classes;

use Contao\Controller;
use Contao\StringUtil;
use Memo\CategoryBundle\Model\CategoryModel;
use Memo\FoundationBundle\Service\ToolboxService;
use Memo\PortfolioBundle\Model\PortfolioArchiveModel;
use Memo\PortfolioBundle\Model\PortfolioModel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

/**
 * Class Importer
 * @package Memo\PortfolioBundle\Classes
 */
class Importer
{
    private $io;

    public function __construct($io = null)
    {
        $this->io = $io;
    }

    /**
     * @param $strFile
     * @return false|void
     */
    public function import($strFile)
    {
        // Check if File exists
        if (!file_exists($strFile)) {
            return false;
        }

        // Get Data from file
        if (!$arrData = self::importFile($strFile)) {
            return false;
        }

        return true;
    }

    /**
     * @param $strFile
     * @return void
     * @throws \Exception
     */
    public function importFile($strFile)
    {

        $this->io->section("Import started");

        // Get the archive
        $objPortfolioArchive = PortfolioArchiveModel::findOneBy('published', 1);

        // Detect File-Type
        $strFileType = pathinfo($strFile, PATHINFO_EXTENSION);

        // Declare empty array
        $arrData = array();
        $arrMapping = array();
        $bolFirstLine = true;
        $intNewAttributeValues = 0;

        // Filetype management
        switch ($strFileType) {
            case 'xls':
            case 'xml':
            case 'csv':
            case 'xlsx':
                // All ok
                break;
            default:
                throw new \Exception('Filetype not supported (yet) -> make a pullrequest on at https://bitbucket.org/memo_development/contao-portfolio-bundle');
        }

        // Load File
        $strOfficeFileType = IOFactory::identify($strFile);
        $objReader = IOFactory::createReader($strOfficeFileType);
        $objReader->setReadDataOnly(true);

        try {
            $objSpreadsheet = $objReader->load($strFile);
        } catch (Exception $e) {
            throw new \Exception('Error loading file "' . pathinfo($strFile, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        // Get worksheet
        $objWorksheet = $objSpreadsheet->getActiveSheet();
        $this->io->progressStart((int)$objWorksheet->getHighestRow() - 1);

        // Loop the rows
        foreach ($objWorksheet->getRowIterator() as $objRow) {



            $intRow = $objRow->getRowIndex();
            $cellIterator = $objRow->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);

            if ($bolFirstLine) {

                $arrMappings = self::mapFileToDatabase($cellIterator);

                $bolFirstLine = false;
                continue;
            }

            // Output advance
            $this->io->progressAdvance();

            // @todo: check if there is an existing product and update it instead of creating a new one

            // Case new product
            $objPortfolio = new PortfolioModel();
            $objPortfolio->tstamp = time();
            $objPortfolio->pid = $objPortfolioArchive->id;
            $objPortfolio->date = time();
            $objPortfolio->published = 1;

            // Create the products
            foreach ($arrMappings as $strColumn => $arrMapping) {

                $bolMultiple = false;
                $bolFKResolved = false;
                $strValue = $objWorksheet->getCell($strColumn . $intRow)->getCalculatedValue();
                $strField = $arrMapping['field'];

                // Format Lists (if needed)
                if (array_key_exists('list', $arrMapping) && $arrMapping['list'] && stristr($strValue, "\n")) {
                    $arrValues = explode("\n", $strValue);
                    $strValue = "<p><ul>";
                    if (is_array($arrValues) && count($arrValues) > 0) {
                        foreach ($arrValues as $strValueItem) {
                            $strValue .= "<li>" . $strValueItem . "</li>";
                        }
                    }
                    $strValue .= "</ul></p>";
                }

                // Format HTML (if needed)
                if (array_key_exists('html', $arrMapping) && $arrMapping['html']) {
                    $strValue = "<p>" . nl2br($strValue) . "</p>";
                }

                // If the whole colum represents a value of a select, use the value of the column, not the value of the cell
                if (array_key_exists('value', $arrMapping)) {
                    if (in_array($strValue, array('x', 'ja', 'yes', 'true', '1', 'Ja'))) {
                        $strValue = $arrMapping['value'];
                        $bolFKResolved = true;
                    } else {
                        continue;
                    }
                }

                // Detect if its a mandatory field
                if (array_key_exists('dca', $arrMapping) && array_key_exists('eval', $arrMapping['dca']) && array_key_exists('multiple', $arrMapping['dca']['eval']) && $arrMapping['dca']['eval']['multiple']) {
                    $bolMultiple = true;
                }

                if (array_key_exists('dca', $arrMapping) && array_key_exists('inputType', $arrMapping['dca']) && $arrMapping['dca']['inputType'] == 'multiColumnWizard') {
                    $bolMultiple = true;
                }

                // If the field is a select-option, save the foreign key instead of the value
                if ($strField == 'categories' || (array_key_exists('dca', $arrMapping) && array_key_exists('options', $arrMapping['dca']))) {

                    if ($bolFKResolved) {
                        $strValueFK = $strValue;
                    } else {
                        $strValueFK = array_search($strValue, $arrMapping['dca']['options']);
                    }

                    if (!$strValueFK && $strField != 'categories') {

                        $strValue = trim($strValue);
                        $strValueFK = array_search($strValue, $arrMapping['dca']['options']);

                        if ($strValueFK === '' || $strValueFK === ' ') {
                            continue;
                        }

                        if (!$strValueFK && $strValueFK !== 0) {

                            if (array_key_exists($strValue, $arrMapping['dca']['options'])) {
                                $strValueFK = $strValue;
                            } else {
                                throw new \Exception('Value "' . $strValue . '" not found in options for field "' . $strField . '"');

                            }

                        }

                    }

                    if ($bolMultiple) {
                        if (!is_array($objPortfolio->$strField)) {
                            $arrValue = array();
                        } else {
                            $arrValue = $objPortfolio->$strField;
                        }

                        if ($strField == 'categories') {
                            $arrValue[] = array('category_id' => $strValueFK);
                        } else {
                            $arrValue[] = $strValueFK;
                        }

                        $objPortfolio->$strField = $arrValue;
                    } else {

                        $objPortfolio->$strField = $strValueFK;
                    }

                } else {
                    if ($strValue != '' && !is_null($strValue)) {
                        $objPortfolio->$strField = $strValue;
                    }
                }
            }

            if ($strSlug = StringUtil::generateAlias($objPortfolio->title)) {
                $objPortfolio->alias = $strSlug;
            }

            // Save the product
            $objPortfolio->save();

            // Is coordinate generation needed? (set on parent archive)
            if ($objPortfolioArchive && $objPortfolioArchive->generateCoordinates) {
                ToolboxService::setCoordinates(false,'tl_memo_portfolio',$objPortfolio);
            }

            // Is marker generation needed? (set on parent archive)
            if ($objPortfolioArchive && $objPortfolioArchive->addMarker) {
                ToolboxService::generateMarker(false, 'tl_memo_portfolio', $objPortfolio);
            }

        }

        $this->io->progressFinish();

        $this->io->newLine();
        $this->io->success("Import finished");

        return $arrData;
    }

    /**
     * @param $objCellIterator
     * @return array
     */
    public function mapFileToDatabase($objCellIterator)
    {

        // Load DCA
        Controller::loadDataContainer('tl_memo_portfolio');
        $arrDCA = $GLOBALS['TL_DCA']['tl_memo_portfolio']['fields'];

        // Load Categories
        $arrCategories = array();
        if ($colCategories = CategoryModel::findAll()) {
            while ($colCategories->next()) {
                $arrCategories[$colCategories->id] = $colCategories->title;
            }
        }

        // Declare empty array
        $arrMapping = array();

        // Loop all columns in first row
        foreach ($objCellIterator as $objCell) {

            // Get the column name
            $strID = $objCell->getColumn();
            $strColumn = $objCell->getValue();

            // Ignore empty columns
            if ($strColumn == '' || $strColumn == ' ') {
                continue;
            }

            $arrMapping[$strID]['field'] = false;
            $arrMapping[$strID]['file_column'] = $strColumn;
            $arrMapping[$strID]['file_column_cleaned'] = $strColumn;

            // Detect List Fields
            $arrMapping[$strID]['list'] = false;
            if (stristr($strColumn, '[LIST]')) {
                $strColumn = str_replace('[LIST]', '', $strColumn);
                $strColumn = rtrim($strColumn);
                $arrMapping[$strID]['list'] = true;
            }

            // Detect HTML Fields
            $arrMapping[$strID]['html'] = false;
            if (stristr($strColumn, '[HTML]')) {
                $strColumn = str_replace('[HTML]', '', $strColumn);
                $strColumn = rtrim($strColumn);
                $arrMapping[$strID]['html'] = true;
            }

            // Detect DCA Fields
            if (array_key_exists($strColumn, $arrDCA)) {
                $arrMapping[$strID]['field'] = $strColumn;
                $arrMapping[$strID]['dca'] = $arrDCA[$strColumn];
                continue;
            }

            // Declutter Multiple-choice fields
            if (stristr($strColumn, '::')) {
                $arrPieces = explode(' :: ', $strColumn);
                $strColumn = $arrPieces[0];
                $strValue = $arrPieces[1];

                if (array_key_exists($strColumn, $arrDCA)) {
                    $arrMapping[$strID]['field'] = $strColumn;
                    $arrMapping[$strID]['dca'] = $arrDCA[$strColumn];
                }

                $bolValueExists = false;
                if (array_key_exists('options', $arrMapping[$strID]['dca'])) {

                    if (array_key_exists($strValue, $arrMapping[$strID]['dca']['options'])) {
                        $bolValueExists = true;
                        $arrMapping[$strID]['value'] = $strValue;
                        continue;
                    }
                }

                if (!$bolValueExists) {

                    if ($strColumn == 'categories' && array_key_exists($strValue, $arrCategories)) {
                        $arrMapping[$strID]['value'] = $strValue;
                        continue;
                    }
                    throw new \Exception('Value "' . $strValue . '" does not exist in field "' . $strColumn . '"');
                }
            }

            // Check if it is a default field
            switch ($strColumn) {
                case 'titel':
                case 'Titel':
                case 'title':
                case 'Title':
                case 'name':
                case 'Name':
                    $arrMapping[$strID]['field'] = 'title';
                    $arrMapping[$strID]['dca'] = $GLOBALS['TL_DCA']['tl_memo_portfolio']['fields']['title'];
                    break;
                case 'beschreibung':
                case 'Beschreibung':
                case 'description':
                case 'Description':
                    $arrMapping[$strID]['field'] = 'description';
                    $arrMapping[$strID]['dca'] = $GLOBALS['TL_DCA']['tl_memo_portfolio']['fields']['description'];
                    break;
                case 'teaser':
                case 'Teaser':
                    $arrMapping[$strID]['field'] = 'teaser';
                    $arrMapping[$strID]['dca'] = $GLOBALS['TL_DCA']['tl_memo_portfolio']['fields']['teaser'];
                    break;
                default:
                    throw new \Exception('Field "' . $strColumn . '" does not exist');
                    break;
            }

            $arrMapping[$strID]['file_column_cleaned'] = $strColumn;
        }

        return $arrMapping;
    }
}
