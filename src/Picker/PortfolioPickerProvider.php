<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\Picker;

use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\CoreBundle\Picker\AbstractInsertTagPickerProvider;
use Contao\CoreBundle\Picker\DcaPickerProviderInterface;
use Contao\CoreBundle\Picker\PickerConfig;
use Knp\Menu\FactoryInterface;
use Memo\PortfolioBundle\Model\PortfolioArchiveModel;
use Memo\PortfolioBundle\Model\PortfolioModel;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class PortfolioPickerProvider extends AbstractInsertTagPickerProvider implements DcaPickerProviderInterface, FrameworkAwareInterface
{

    use FrameworkAwareTrait;

    public function __construct(
        FactoryInterface $menuFactory,
        RouterInterface $router,
        TranslatorInterface|null $translator,
        private readonly Security $security,
    ) {
        parent::__construct($menuFactory, $router, $translator);
    }

    public function getName(): string
    {

        return 'portfolioPicker';
    }


    public function supportsContext($context): bool
    {
        return 'link' === $context;
    }


    public function supportsValue(PickerConfig $config): bool
    {

        return $this->isMatchingInsertTag($config);
    }

    public function getDcaTable(PickerConfig|null $config = null): string
    {
        return 'tl_memo_portfolio';
    }

    public function getDcaAttributes(PickerConfig $config): array
    {

        $attributes = ['fieldType' => 'radio'];

        if ($this->supportsValue($config)) {
            $attributes['value'] = $this->getInsertTagValue($config);

            if ($flags = $this->getInsertTagFlags($config)) {
                $attributes['flags'] = $flags;
            }
        }

        return $attributes;
    }


    public function convertDcaValue(PickerConfig $config, $value): string
    {
        return sprintf($this->getInsertTag($config), $value);
    }


    protected function getRouteParameters(PickerConfig $config = null): array
    {
        $params = ['do' => 'memo_portfolio'];

        if (null === $config || !$config->getValue() || !$this->supportsValue($config)) {
            return $params;
        }

        if (null !== ($intItemId = $this->getItemId($this->getInsertTagValue($config)))) {
            $params['table'] = 'tl_memo_portfolio';
            $params['id'] = $intItemId;
        }

        return $params;
    }

    protected function getDefaultInsertTag(): string
    {
        return '{{portfolio_url::%s}}';
    }

    private function getItemId(int|string $id): int|null
    {
        $eventAdapter = $this->framework->getAdapter(PortfolioModel::class);

        if (!($ItemModel = $eventAdapter->findByPk($id)) instanceof PortfolioModel) {
            return null;
        }

        if (!($itemArchive = $ItemModel->getRelated('pid')) instanceof PortfolioArchiveModel) {
            return null;
        }

        return (int) $itemArchive->id;
    }
}
