<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\Module;

use Memo\FoundationBundle\Module\FoundationModule;
use Memo\PortfolioBundle\Model\PortfolioModel;
use Memo\ProductBundle\Model\ProductModel;
use Terminal42\ChangeLanguage\PageFinder;
use Contao\PageModel;

class ModulePortfolioListing extends FoundationModule
{
    protected $strTemplate = 'mod_portfolio_listing';
}
