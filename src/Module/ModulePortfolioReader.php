<?php declare(strict_types=1);

/**
 * @package   Memo\MemoPortfolioBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\PortfolioBundle\Module;

use Contao\Input;
use Memo\FoundationBundle\Module\FoundationModule;
use Memo\PortfolioBundle\Model\PortfolioModel;

class ModulePortfolioReader extends FoundationModule
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_portfolio_reader';

    /**
     * Generate module
     */
    protected function compile()
    {
        // Get auto_item (GET Parameter)
        $strAlias = Input::get('auto_item');

        // Check if there is an Alias in the URL and get the item by alias (this also filters by start/stop, looks for alias_en etc.)
        if ($strAlias !== '' && $colItem = PortfolioModel::getItemByTranslatedAlias($strAlias)) {

            // Set the MetaData for the detailpage
            $this->setMetaData($colItem);

            // Parse the item into an array (this also generates images, html-template etc.)
            $arrItems = $this->parseItems($colItem, null, false);

            // Define the data "raw" for custom template use
            $this->Template->setData($colItem->row());

            // Set the custom template, if defined
            if ($this->customTpl) {
                $this->Template->strTemplate = $this->customTpl;
            }

            // Pass the items to the template
            $this->Template->items = $arrItems;
        }
    }
}
