# Memo Portfolio Bundle


## Table of contents

> * [Memo Portfolio Bundle](#title--repository-name)
>   * [About](#about)
>   * [Table of contents](#table-of-contents)
>   * [Installation](#installation)
>   * [Usage](#usage)
>   * [Features](#features)
>     * [Import](#import)
>   * [Hooks](#hooks)
>   * [Requirements](#requirements)
>   * [Contributing / Reporting issues](#contributing--reporting-issues)
>   * [License](#license)

## About

With the Portfolio Bundle you can add different list and detail-views of projects, references, etc..

## Installation

Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repos (not on packagist.org) to your composer.json:

```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-foundation-bundle"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-category-bundle"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-portfolio-bundle"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-foundation-bundle": "^2.0",
"memo_development/contao-category-bundle": "^2.0",
"memo_development/contao-portfolio-bundle": "^2.0",
```

Update the database via contao install tool or console:

```
./update-db.sh
```

## Usage
* Create a portfolio-listing page
* Create a portfolio-detail page
* Create a new portfolio-archive and link both pages
* Create a new project/reference in the portfolio-archive
* Create a new portfolio-listing content-element or module (and configure it)
* Create a new portfolio-detail-module
* Give the backend users rights to manage the portfolio-archives
* Optional: Create a copy of templates (portfolio_item, portfolio_item_full) and modify them
* Optional: Setup a Portfolio-Customize Bundle to customize the dca and logic (via Hooks)

## Features
* Backend management of projects, mostly related to locations (with name, description, image, gallery, address, long/lat, category, etc.)
* List of all projects in one or more archives
* List of all projects by category
* List of all projects by custom/manual selection
* List of all projects by sql statement
* As Element or Module
* Contao URL-Picker for easy linking to detailpages
* Auto include team-bundle as "people involved"
* Auto add generated detailpages to xml-sitemap
* Detailpage of a project
* Marker generation for maps
* Possible Source for a map - https://bitbucket.org/memo_development/contao-map-bundle
* Import:

### Import
A simple import has been prepared (you are invited to extend it ;).
This is based on the PhpSpreadsheet Lib and can import XLS, XLSX and CSV files.
The first line must contain either the portfolio columnname, for example title, address, etc.
For multiple selection possibilities you can make one column per value, e.g. colour :: red, colour :: blue, colour :: green, etc.
The values (here red, blue, green) must be created beforehand in the Contao backend and be exactly the same.

You can then import with the following command:

```
vendor/bin/contao-console portfolio:import real_import_example.xlsx
```

The error messages should then be meaningful enough to be analysed. ;)  
Best you test it, before sending the prepared xlsx to the customer.  
The Import does *not* update entries, it always creates new ones - so only onetime use.

## Hooks
* generatePortfolioDCA - Customize DCA (add, remove or change fields/config) **before** the fields get multiplied for translation (so no title_en, title_fr yet), expects the DCA as array as return value
* generatePortfolioDCAComplete - Customize the final DCA (remove fields, change fields) **after** the language fields have been generated (so title_en, title_fr are available), expects the DCA as array as return value
* addItemToSitemap - Customize how and if urls are added to the sitemap, expects the url as string as return value (or false, to remove the url)

## Requirements

* PHP 8.1+ (older versions with v1)
* Contao 5.2+ (older versions with v1)
* Foundation-Bundle
* Category-Bundle

## Contributing / Reporting issues

Bug reports and pull requests are welcome - via Bitbucket-Issues:
[Bitbucket Issues](https://bitbucket.org/memo_development/contao-portfolio-bundle/issues?status=new&status=open)

## License

[GNU Lesser General Public License, Version 3.0](https://www.gnu.de/documents/lgpl-3.0.de.html)
